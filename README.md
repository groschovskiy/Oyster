# Oyster - 

Demonstration Edition

Transport for London (TfL) is a local government body responsible for most aspects of the transport system in Greater London in England. Its role is to implement the transport strategy and to manage transport services across London. Its head office is in Windsor House in the City of Westminster.

TfL was created in 2000 as part of the Greater London Authority by the Greater London Authority Act 1999. It gained most of its functions from its predecessor London Regional Transport in 2000. The first Commissioner of TfL was Bob Kiley. The first Chair was then-Mayor of London Ken Livingstone, and the first Vice-Chair was Dave Wetzel. Livingstone and Wetzel remained in office until the election of Boris Johnson as Mayor in 2008.

TfL did not take over responsibility for the London Underground until 2003, after the controversial Public-private partnership (PPP) contract for maintenance had been agreed. Management of the Public Carriage Office had previously been a function of the Metropolitan Police.

Transport for London Group Archives holds business records for TfL and its predecessor bodies and transport companies. Some early records are also held on behalf of TfL Group Archives at the London Metropolitan Archives.

After the bombings on the underground and bus systems on 7 July 2005, many staff were recognised in the 2006 New Year honours list for the work they did. They helped survivors out, removed bodies, and got the transport system up and running, to get the millions of commuters back out of London at the end of the work day. Those mentioned include Peter Hendy, who was at the time Head of Surface Transport division, and Tim O'Toole, head of the Underground division, who were both awarded CBEs. Others included David Boyce, Station Supervisor, London Underground (MBE); John Boyle, Train Operator, London Underground (MBE); Peter Sanders, Group Station Manager, London Underground (MBE); Alan Dell, Network Liaison Manager, London Buses (MBE) and John Gardner, Events Planning Manager (MBE).

On 1 June 2008, the drinking of alcoholic beverages was banned on Tube and London Overground trains, buses, trams, Docklands Light Railway and all stations operated by TfL across London but not those operated by other rail companies. Carrying open containers of alcohol was also banned on public transport operated by TfL. The Mayor of London and TfL announced the ban with the intention of providing a safer and more pleasant experience for passengers. There were "Last Round on the Underground" parties on the night before the ban came into force. Passengers refusing to observe the ban may be refused travel and asked to leave the premises. The Greater London Authority reported in 2011 that assaults on London Underground staff had fallen by 15% since the introduction of the ban.

TfL commissioned a survey in 2013 which showed that 15% of women using public transport in London had been the subject of some form of unwanted sexual behaviour but that 90% of incidents were not reported to the police. In an effort to reduce sexual offences and increase reporting, TfL—in conjunction with the British Transport Police, Metropolitan Police Service, and City of London Police—launched Project Guardian.
