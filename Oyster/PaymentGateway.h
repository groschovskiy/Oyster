//
//  PaymentGateway.h
//  Oyster
//
//  Created by Dmitriy Groschovskiy on 26.06.15.
//  Copyright (c) 2015 Transport for London. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentGateway : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIButton *makePaymentButton;
@property (strong, nonatomic) IBOutlet UIButton *makePurchaseVoidVisaCVVButton;
@property (strong, nonatomic) IBOutlet UIButton *makeAuthorizeAmexCVVButton;
@property (strong, nonatomic) IBOutlet UIButton *makeAuthorizeCaptureVisaCVVButton;
@property (strong, nonatomic) IBOutlet UIButton *makePurchaseRefundVisaTransactionCVVButton;

@property (strong, nonatomic) IBOutlet UITextField *amountEntered;
@property (strong, nonatomic) IBOutlet UITextField *card_holder_name;
@property (strong, nonatomic) IBOutlet UITextField *card_number;
@property (strong, nonatomic) IBOutlet UITextField *card_security_code;
@property (strong, nonatomic) IBOutlet UITextField *card_expiration_date;

- (IBAction)authorizeCaptureVisaTransactionCVV:(id)sender;

@end
