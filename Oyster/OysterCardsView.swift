//
//  OysterCardsView.swift
//  Oyster
//
//  Created by Groschovskiy Dmitriy on 21.06.15.
//  Copyright (c) 2015 Transport for London. All rights reserved.
//

import UIKit
import Parse

class OysterCardsView: UIViewController {
    
    @IBOutlet var oysterNumber : UILabel!
    @IBOutlet var oysterFullName : UILabel!
    @IBOutlet var oysterCurrentCredit : UILabel!
    @IBOutlet var oysterExpireDate : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadOysterCardInformation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true)
        println("Developer Hub: Hide keyboard action executed")
    }

    // MARK: - Show Oyster Card Information

    func loadOysterCardInformation() {
        var currentUser = PFUser.currentUser()?.objectId
        var requestPaymentProfile = PFQuery(className:"_User")
        requestPaymentProfile.getObjectInBackgroundWithId(currentUser!) {
            (paymentInformation: PFObject?, error: NSError?) -> Void in
            if error == nil && paymentInformation != nil {
                let firstNameJSON = paymentInformation?["firstName"] as! String
                let lastNameJSON = paymentInformation?["lastName"] as! String
                self.oysterFullName.text = "\(firstNameJSON) \(lastNameJSON)"
                let oysterCardNumberJSON = paymentInformation?["transportNumber"] as! String
                
                var ticketServiceRequest = PFQuery(className:"UKTicket")
                ticketServiceRequest.getObjectInBackgroundWithId(oysterCardNumberJSON) {
                    (ticketService: PFObject?, error: NSError?) -> Void in
                    if error == nil && ticketService != nil {
                        let creditCardNumberJSON = ticketService?["creditCardNumber"] as! String
                        self.oysterNumber.text = creditCardNumberJSON
                        let expirationNumberJSON = ticketService?["expirationDate"] as! String
                        self.oysterExpireDate.text = expirationNumberJSON

                        let currentCreditJSON = paymentInformation?["currentCredit"] as! String
                        let currentCreditMSQL = (currentCreditJSON as NSString).floatValue
                        let currentCreditXMS = String(format: "%.2f", currentCreditMSQL)
                        self.oysterCurrentCredit.text = "Your current credit: £\(currentCreditXMS)"

                        println(ticketService)
                    } else {
                        println(error)
                    }
                }
                println(paymentInformation)
            } else {
                println(error)
            }
        }
    }
    
    @IBAction func postManualRequest(sender: UIButton) {
        
    }
    
    // MARK: - Close View Controller
    
    @IBAction func closeCurrentViewController() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
