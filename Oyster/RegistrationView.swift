//
//  RegistrationView.swift
//  Oyster
//
//  Created by Groschovskiy Dmitriy on 21.06.15.
//  Copyright (c) 2015 Transport for London. All rights reserved.
//

import UIKit
import Parse

class RegistrationView: UIViewController {

    @IBOutlet var username : UITextField!
    @IBOutlet var password : UITextField!
    @IBOutlet var firstName : UITextField!
    @IBOutlet var lastName : UITextField!
    @IBOutlet var oysterNumber : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }

    // MARK: - Source Code

    @IBAction func showRegistrationController() {
            var user = PFUser()
            user.username = username.text
            user.password = password.text
            user.email = username.text

            user["currentCredit"] = "9.99"
            user["firstName"] = firstName.text
            user["lastName"] = lastName.text
            user["oysterCardNumber"] = oysterNumber.text
            user["passengerClass"] = "Classic"
            user["passengerNotification"] = "Standart pricing terms"

            user.signUpInBackgroundWithBlock {
                (succeeded: Bool, error: NSError?) -> Void in
                if let error = error {
                    let errorString = error.userInfo?["error"] as? NSString
                } else {
                    let viewController : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ManagementController") as! UIViewController
                    self.presentViewController(viewController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func returnToHome() {
        dismissViewControllerAnimated(true, completion: nil)
    }
}
