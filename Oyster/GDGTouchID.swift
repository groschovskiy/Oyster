//
//  GDGTouchID.swift
//  Google Touch ID Framework for Authorize Users
//
//  Created by Dmitriy Groschovskiy on 05/06/15.
//  Copyright (c) 2015 Google Inc. All rights reserved.
//

import UIKit
import LocalAuthentication

class GDGTouchID {
    
    func authenticateUser(success succeed: (() -> ())? = nil, failure fail: (NSError -> ())? = nil) {
        if fail == nil && succeed == nil { return }
        
        let context : LAContext = LAContext()
        var error : NSError?
        var myLocalizedReasonString : NSString = "Authentification is required"
        
        if context.canEvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, error: &error) {
            context.evaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, localizedReason: myLocalizedReasonString as String, reply: { (success : Bool, evaluationError : NSError?) -> Void in
                if success {
                    if let succeed = succeed {
                        dispatch_async(dispatch_get_main_queue()) {
                            succeed()
                        }
                    }
                } else {
                    if let fail = fail {
                        dispatch_async(dispatch_get_main_queue()) {
                            fail(evaluationError!)
                        }
                    }
                }
            })
        } else {
            if let fail = fail {
                dispatch_async(dispatch_get_main_queue()) {
                    fail(error!)
                }
            }
        }
    }
    
    func fetchImage(failure fail : (NSError -> ())? = nil,
        success succeed: (UIImage -> ())? = nil) {
    }
}