//
//  ManagementView.swift
//  Oyster
//
//  Created by Groschovskiy Dmitriy on 19.06.15.
//  Copyright (c) 2015 Transport for London. All rights reserved.
//

import UIKit
import Parse
import Foundation

class ManagementView: UIViewController {
    
    @IBOutlet var firstName : UILabel!
    @IBOutlet var lastName : UILabel!
    @IBOutlet var currentCredit : UILabel!
    @IBOutlet var currentBonusUnderground : UILabel!
    @IBOutlet var currentBonusOverground : UILabel!
    @IBOutlet var profilePhotoImage : UIImageView!
    @IBOutlet var passengerStatus : UILabel!
    @IBOutlet var passengerNotification : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewCustomerInformationInDatabase()

        self.profilePhotoImage.layer.cornerRadius = self.profilePhotoImage.frame.size.width / 2
        self.profilePhotoImage.clipsToBounds = true
        var tap = UITapGestureRecognizer(target: self, action: Selector("profilePhotoTapEvent"))
        profilePhotoImage.userInteractionEnabled = true

         var timer = NSTimer.scheduledTimerWithTimeInterval(7.5, target: self, selector: Selector("viewCustomerInformationInDatabase"), userInfo: nil, repeats: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true)
        println("Developer Hub: Hide keyboard action executed")
    }

    // MARK: - Load and Show Customer Information

    func viewCustomerInformationInDatabase() {
        let cPlusPlus : GDGWallet = GDGWallet()
        
        var currentUser = PFUser.currentUser()?.objectId
        var query = PFQuery(className: "_User")
        query.getObjectInBackgroundWithId(currentUser!) {
            (customerAccount: PFObject?, error: NSError?) -> Void in
            if error == nil && customerAccount != nil {
                let firstNameJSON = customerAccount?["firstName"] as! String
                self.firstName.text = firstNameJSON
                let lastNameJSON = customerAccount?["lastName"] as! String
                self.lastName.text = lastNameJSON
                let transportNumber = customerAccount?["transportNumber"] as! String

                var currentUser = PFUser.currentUser()?.objectId
                var request = PFQuery(className: "UKTicket")
                request.getObjectInBackgroundWithId(transportNumber) {
                    (customerCard: PFObject?, error: NSError?) -> Void in
                    if error == nil && customerCard != nil {
                        let undergroundBonusTripsJSON = customerCard?["bonusUndergroundTrips"] as! String
                        self.currentBonusUnderground.text = undergroundBonusTripsJSON
                        let overgroundBonusTripsJSON = customerCard?["bonusOvergroundTrips"] as! String
                        self.currentBonusOverground.text = overgroundBonusTripsJSON
                        let currentAccountCreditJSON = customerCard?["currentCredit"] as! String
                        let currentAccountCreditFloat = (currentAccountCreditJSON as NSString).floatValue
                        let currentAccountCreditString = "£ \(currentAccountCreditFloat)"
                        self.currentCredit.text = currentAccountCreditString
                        println(customerCard)
                    } else {
                        println(error)
                    }
                }

                self.loadProfileImageService()

                println(customerAccount)
            } else {
                println(error)
            }
        }
    }
    
    func loadProfileImageService() {
        PFUser.currentUser()?["picture"]
        if let userPicture = PFUser.currentUser()?["profilePicture"] as? PFFile {
            userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                if (error == nil) {
                    self.profilePhotoImage.image = UIImage(data:imageData!)
                }
            }
        }
    }
    
    // MARK: - Show new View Controller
    
    @IBAction func showRefundableController(sender: UIButton) {
        let paymentsController : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("PaymentController") as! UIViewController
        self.presentViewController(paymentsController, animated: true, completion: nil)
    }
    
    // MARK: - QR Code Image Generator
    
    func generateQRImage(stringQR:NSString, withSizeRate rate:CGFloat) -> UIImage
    {
        var filter:CIFilter = CIFilter(name:"CIQRCodeGenerator")
        filter.setDefaults()
        
        var data:NSData = stringQR.dataUsingEncoding(NSUTF8StringEncoding)!
        filter.setValue(data, forKey: "inputMessage")
        
        var outputImg:CIImage = filter.outputImage
        
        var context:CIContext = CIContext(options: nil)
        var cgimg:CGImageRef = context.createCGImage(outputImg, fromRect: outputImg.extent())
        
        var img:UIImage = UIImage(CGImage: cgimg, scale: 1.0, orientation: UIImageOrientation.Up)!
        
        var width  = img.size.width * rate
        var height = img.size.height * rate
        
        UIGraphicsBeginImageContext(CGSizeMake(width, height))
        var cgContxt:CGContextRef = UIGraphicsGetCurrentContext()
        CGContextSetInterpolationQuality(cgContxt, kCGInterpolationNone)
        img.drawInRect(CGRectMake(0, 0, width, height))
        img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img
    }
    
    // MARK: - Metro PCH
    
    func zoneLightJourney() {
        var currentUser = PFUser.currentUser()?.objectId
        var query = PFQuery(className: "_User")
        query.getObjectInBackgroundWithId(currentUser!) {
            (customerAccount: PFObject?, error: NSError?) -> Void in
            if error == nil && customerAccount != nil {
                let firstNameJSON = customerAccount?["firstName"] as! String
                self.firstName.text = firstNameJSON
                let lastNameJSON = customerAccount?["lastName"] as! String
                self.lastName.text = lastNameJSON
                let transportNumber = customerAccount?["transportNumber"] as! String
                
                var currentUser = PFUser.currentUser()?.objectId
                var request = PFQuery(className: "UKTicket")
                request.getObjectInBackgroundWithId(transportNumber) {
                    (customerCard: PFObject?, error: NSError?) -> Void in
                    if error == nil && customerCard != nil {
                        let currentCreditJSON = customerCard?["currentCredit"] as! String
                        var currentCreditRating = (currentCreditJSON as NSString).floatValue
                        var newCreditRating = 2.90 as Float
                        var query = PFQuery(className:"UKTicket")
                        query.getObjectInBackgroundWithId(transportNumber) {
                            (purchaseEvent: PFObject?, error: NSError?) -> Void in
                            if error != nil {
                                println(error)
                            } else if let purchaseEvent = purchaseEvent {
                                var resultCreditRating = currentCreditRating - newCreditRating
                                let resultCreditSummary = NSString(format: "%f.2", resultCreditRating)
                                purchaseEvent["currentCredit"] = resultCreditSummary
                                purchaseEvent.saveInBackground()
                            }
                        }
                        println(customerCard)
                    } else {
                        println(error)
                    }
                }
                println(customerAccount)
            } else {
                println(error)
            }
        }
    }
    
    func zonePrimeJourney() {
        var currentUser = PFUser.currentUser()?.objectId
        var query = PFQuery(className: "_User")
        query.getObjectInBackgroundWithId(currentUser!) {
            (customerAccount: PFObject?, error: NSError?) -> Void in
            if error == nil && customerAccount != nil {
                let firstNameJSON = customerAccount?["firstName"] as! String
                self.firstName.text = firstNameJSON
                let lastNameJSON = customerAccount?["lastName"] as! String
                self.lastName.text = lastNameJSON
                let transportNumber = customerAccount?["transportNumber"] as! String
                
                var currentUser = PFUser.currentUser()?.objectId
                var request = PFQuery(className: "UKTicket")
                request.getObjectInBackgroundWithId(transportNumber) {
                    (customerCard: PFObject?, error: NSError?) -> Void in
                    if error == nil && customerCard != nil {
                        let currentCreditJSON = customerCard?["currentCredit"] as! String
                        var currentCreditRating = (currentCreditJSON as NSString).floatValue
                        var newCreditRating = 5.10 as Float
                        var query = PFQuery(className:"UKTicket")
                        query.getObjectInBackgroundWithId(transportNumber) {
                            (purchaseEvent: PFObject?, error: NSError?) -> Void in
                            if error != nil {
                                println(error)
                            } else if let purchaseEvent = purchaseEvent {
                                var resultCreditRating = currentCreditRating - newCreditRating
                                let resultCreditSummary = NSString(format: "%f.2", resultCreditRating)
                                purchaseEvent["currentCredit"] = resultCreditSummary
                                purchaseEvent.saveInBackground()
                            }
                        }
                        println(customerCard)
                    } else {
                        println(error)
                    }
                }
                println(customerAccount)
            } else {
                println(error)
            }
        }
    }
    
    func busPrimeJourney() {
        var currentUser = PFUser.currentUser()?.objectId
        var query = PFQuery(className: "_User")
        query.getObjectInBackgroundWithId(currentUser!) {
            (customerAccount: PFObject?, error: NSError?) -> Void in
            if error == nil && customerAccount != nil {
                let firstNameJSON = customerAccount?["firstName"] as! String
                self.firstName.text = firstNameJSON
                let lastNameJSON = customerAccount?["lastName"] as! String
                self.lastName.text = lastNameJSON
                let transportNumber = customerAccount?["transportNumber"] as! String
                
                var currentUser = PFUser.currentUser()?.objectId
                var request = PFQuery(className: "UKTicket")
                request.getObjectInBackgroundWithId(transportNumber) {
                    (customerCard: PFObject?, error: NSError?) -> Void in
                    if error == nil && customerCard != nil {
                        let currentCreditJSON = customerCard?["currentCredit"] as! String
                        var currentCreditRating = (currentCreditJSON as NSString).floatValue
                        var newCreditRating = 1.50 as Float
                        var query = PFQuery(className:"UKTicket")
                        query.getObjectInBackgroundWithId(transportNumber) {
                            (purchaseEvent: PFObject?, error: NSError?) -> Void in
                            if error != nil {
                                println(error)
                            } else if let purchaseEvent = purchaseEvent {
                                var resultCreditRating = currentCreditRating - newCreditRating
                                let resultCreditSummary = NSString(format: "%f.2", resultCreditRating)
                                purchaseEvent["currentCredit"] = resultCreditSummary
                                purchaseEvent.saveInBackground()
                            }
                        }
                        println(customerCard)
                    } else {
                        println(error)
                    }
                }
                println(customerAccount)
            } else {
                println(error)
            }
        }

    }
    
    func unlimitedJourney() {
        var currentUser = PFUser.currentUser()?.objectId
        var query = PFQuery(className: "_User")
        query.getObjectInBackgroundWithId(currentUser!) {
            (customerAccount: PFObject?, error: NSError?) -> Void in
            if error == nil && customerAccount != nil {
                let firstNameJSON = customerAccount?["firstName"] as! String
                self.firstName.text = firstNameJSON
                let lastNameJSON = customerAccount?["lastName"] as! String
                self.lastName.text = lastNameJSON
                let transportNumber = customerAccount?["transportNumber"] as! String
                
                var currentUser = PFUser.currentUser()?.objectId
                var request = PFQuery(className: "UKTicket")
                request.getObjectInBackgroundWithId(transportNumber) {
                    (customerCard: PFObject?, error: NSError?) -> Void in
                    if error == nil && customerCard != nil {
                        let currentCreditJSON = customerCard?["currentCredit"] as! String
                        var currentCreditRating = (currentCreditJSON as NSString).floatValue
                        var newCreditRating = 6.40 as Float
                        var query = PFQuery(className:"UKTicket")
                        query.getObjectInBackgroundWithId(transportNumber) {
                            (purchaseEvent: PFObject?, error: NSError?) -> Void in
                            if error != nil {
                                println(error)
                            } else if let purchaseEvent = purchaseEvent {
                                var resultCreditRating = currentCreditRating - newCreditRating
                                let resultCreditSummary = NSString(format: "%f.2", resultCreditRating)
                                purchaseEvent["currentCredit"] = resultCreditSummary
                                purchaseEvent.saveInBackground()
                            }
                        }
                        println(customerCard)
                    } else {
                        println(error)
                    }
                }
                println(customerAccount)
            } else {
                println(error)
            }
        }
    }
    
    @IBAction func generateNewQR(sender: UIButton!) {
        // Create the alert controller
        var alertController = UIAlertController(title: "Transport for London", message: "Please select your travel credit option for purchase correct ticket.", preferredStyle: .Alert)
        
        // Create the actions
        var zoneLightJourney = UIAlertAction(title: "Zones 1 to 2 - £2.90", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            self.zoneLightJourney()
            var ticketPassbook = NSUUID().UUIDString
            println(ticketPassbook)
            let alertViewController : UIAlertView = UIAlertView(title: "Transport for London", message: "Your private identification passenger number: \(ticketPassbook). Lean your mobile phone or smart watches the turnstiles for the authorization ticket.", delegate: nil, cancelButtonTitle: "Okay")
            alertViewController.show()
            self.viewCustomerInformationInDatabase()
        }
        
        var zonePrimeJourney = UIAlertAction(title: "Zones 1 to 6 - £5.10", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            self.zonePrimeJourney()
            var ticketPassbook = NSUUID().UUIDString
            println(ticketPassbook)
            let alertViewController : UIAlertView = UIAlertView(title: "Transport for London", message: "Your private identification passenger number: \(ticketPassbook). Lean your mobile phone or smart watches the turnstiles for the authorization ticket.", delegate: nil, cancelButtonTitle: "Okay")
            alertViewController.show()
            self.viewCustomerInformationInDatabase()
        }
        
        var busJourney = UIAlertAction(title: "Bus journey £1.50", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            self.busPrimeJourney()
            var ticketPassbook = NSUUID().UUIDString
            println(ticketPassbook)
            let alertViewController : UIAlertView = UIAlertView(title: "Transport for London", message: "Your private identification passenger number: \(ticketPassbook). Lean your mobile phone or smart watches the turnstiles for the authorization ticket.", delegate: nil, cancelButtonTitle: "Okay")
            alertViewController.show()
            self.viewCustomerInformationInDatabase()
        }
        
        var unlimitedJourney = UIAlertAction(title: "Unlimited journeys - £6.40", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            self.unlimitedJourney()
            var ticketPassbook = NSUUID().UUIDString
            println(ticketPassbook)
            let alertViewController : UIAlertView = UIAlertView(title: "Transport for London", message: "Your private identification passenger number: \(ticketPassbook). Lean your mobile phone or smart watches the turnstiles for the authorization ticket.", delegate: nil, cancelButtonTitle: "Okay")
            alertViewController.show()
            self.viewCustomerInformationInDatabase()
        }
        
        // Add the actions
        alertController.addAction(zoneLightJourney)
        alertController.addAction(zonePrimeJourney)
        alertController.addAction(busJourney)
        alertController.addAction(unlimitedJourney)
        
        // Present the controller
        self.presentViewController(alertController, animated: true, completion: nil)
        self.viewCustomerInformationInDatabase()
    }
}