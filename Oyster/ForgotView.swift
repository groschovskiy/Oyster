//
//  ForgotView.swift
//  Oyster
//
//  Created by Groschovskiy Dmitriy on 21.06.15.
//  Copyright (c) 2015 Transport for London. All rights reserved.
//

import UIKit
import Parse

class ForgotView: UIViewController {
    
    @IBOutlet var username : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true)
        println("Debuger: Keyboard hide event called")
    }
    
    // MARK: - Restore Password Event
    
    @IBAction func returnToHome() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func recoveryAuthentificationEvent(sender: UIButton) {
        PFUser.requestPasswordResetForEmailInBackground(username.text)
        dismissViewControllerAnimated(true, completion: nil)
    }
}
