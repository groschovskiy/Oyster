//
//  ViewController.swift
//  Oyster
//
//  Created by Groschovskiy Dmitriy on 19.06.15.
//  Copyright (c) 2015 Transport for London. All rights reserved.
//

import UIKit
import Parse
import LocalAuthentication

class ViewController: UIViewController {

    @IBOutlet var username : UITextField!
    @IBOutlet var password : UITextField!
    
    enum LAError : Int {
        case AuthenticationFailed
        case UserCancel
        case UserFallback
        case SystemCancel
        case PasscodeNotSet
        case TouchIDNotAvailable
        case TouchIDNotEnrolled
    }
    
    override func viewDidLoad() {
        self.authenticateUser()
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true)
        println("Debuger: Keyboard hide event called")
    }

    // MARK: - Show ViewControllers

    @IBAction func authWithCredentials(sender: UIButton) {
        PFUser.logInWithUsernameInBackground(username.text, password:password.text) {
            (user: PFUser?, error: NSError?) -> Void in
            if user != nil {
                let viewController : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ManagementController") as! UIViewController
                self.presentViewController(viewController, animated: true, completion: nil)
                println("Server: Access Granted")
            } else {
                let alert : UIAlertView = UIAlertView(title: "Transport for London", message: "Your username or password is incorrect! Please check your credentials and try again. Thank you!", delegate: nil, cancelButtonTitle: "Okay")
                alert.show()
                println("Server: Access Denied")
            }
        }
    }

    @IBAction func showRegistrationController() {
        let registrationController : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("registrationController") as! UIViewController
        self.presentViewController(registrationController, animated: true, completion: nil)
    }

    @IBAction func showRecoveryController() {
        let viewController : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("RecoveryController") as! UIViewController
        self.presentViewController(viewController, animated: true, completion: nil)
    }

    // MARK: - Functional builder
    
    func loadOysterPersonalPage() {
        let viewController : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("TicketComposter") as! UIViewController
        self.presentViewController(viewController, animated: true, completion: nil)
        println("Server: Access Granted")
    }
    
    func authenticateUser() {
        let touchIDClass : GDGTouchID = GDGTouchID()
        
        touchIDClass.authenticateUser(success: { () -> () in
            NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                self.loadOysterPersonalPage()
            })}, failure: { (evaluationError: NSError) -> () in
                    switch evaluationError.code {
                    case LAError.SystemCancel.rawValue:
                        println("Authentication cancelled by the system")
                    case LAError.UserCancel.rawValue:
                        println("Authentication cancelled by the user")
                    case LAError.UserFallback.rawValue:
                        println("User wants to use a password")

                    NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                        println("")
                    })
                    case LAError.TouchIDNotEnrolled.rawValue:
                        println("TouchID not enrolled")
                    case LAError.PasscodeNotSet.rawValue:
                        println("Passcode not set")
                    default:
                        println("Authentication failed")
                        NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                            println("")
                })
            }
        })
    }
}

