//
//  PaymentGateway.m
//  Oyster
//
//  Created by Dmitriy Groschovskiy on 26.06.15.
//  Copyright (c) 2015 Transport for London. All rights reserved.
//

#import "PaymentGateway.h"
#import "PayeezySDK.h"
#import <Parse/Parse.h>

@interface PaymentGateway ()

#define KApiKey     @"y6pWAJNyJyjGv66IsVuWnklkKUPFbb0a"
#define KApiSecret  @"86fbae7030253af3cd15faef2a1f4b67353e41fb6799f576b5093ae52901e6f786fbae7030253af3cd15faef2a1f4b67353e41fb6799f576b5093ae52901e6f7"
#define KToken      @"fdoa-a480ce8951daa73262734cf102641994c1e55e7cdf4c02b6"
#define KURL        @"https://api-cert.payeezy.com/v1/transactions"

@end

@implementation PaymentGateway

- (void)viewDidLoad {
    self.card_holder_name.delegate = self;
    self.card_number.delegate = self;
    self.card_security_code.delegate = self;
    self.amountEntered.delegate = self;
    self.card_expiration_date.delegate = self;
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)didCloseController:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)authorizeCaptureVisaTransactionCVV:(id)sender {
    NSString* cardHolderName = _card_holder_name.text;
    NSDecimalNumber* valueEntered = [NSDecimalNumber decimalNumberWithString:_amountEntered.text];
    NSString* cardNumber  = _card_number.text;
    NSString* cardSecurityCode = _card_security_code.text;
    
    if (![valueEntered isEqualToNumber:[NSDecimalNumber notANumber]]) {
        
        NSString *amount = [[NSString stringWithFormat:@"%@",valueEntered] stringByReplacingOccurrencesOfString:@"." withString:@""];
        
        // Test credit card inf
        NSDictionary* credit_card = @{
                                      @"type":@"visa",
                                      @"cardholder_name":cardHolderName,
                                      @"card_number":cardNumber,
                                      @"exp_date":@"0450",
                                      @"cvv":cardSecurityCode
                                      };
        PayeezySDK* myClient = [[PayeezySDK alloc]initWithApiKey:KApiKey apiSecret:KApiSecret merchantToken:KToken url:KURL];
        
        //myClient.url = KURL;
        
        [myClient submitAuthorizeTransactionWithCreditCardDetails:credit_card[@"type"]
                                                   cardHolderName:credit_card[@"cardholder_name"]
                                                       cardNumber:credit_card[@"card_number"]
                                          cardExpirymMonthAndYear:credit_card[@"exp_date"]
                                                          cardCVV:credit_card[@"cvv"]
                                                     currencyCode:@"GBP"
                                                      totalAmount:amount
                                         merchantRefForProcessing:@"abc1412096293369"
         
                                                       completion:^(NSDictionary *dict, NSError *error) {
                                                           
                                                           NSString *authStatusMessage = nil;
                                                           
                                                           if (error == nil)
                                                           {
                                                               authStatusMessage = [NSString stringWithFormat:@"Transaction Successful\rType:%@\rTransaction ID:%@\rTransaction Tag:%@\rCorrelation Id:%@\rBank Response Code:%@",
                                                                                    [dict objectForKey:@"transaction_type"],
                                                                                    [dict objectForKey:@"transaction_id"],
                                                                                    [dict objectForKey:@"transaction_tag"],
                                                                                    [dict objectForKey:@"correlation_id"],
                                                                                    [dict objectForKey:@"bank_resp_code"]];
                                                               [self voidRefundCaptureTransaction:@"abc1412096293369" :[dict objectForKey:@"transaction_tag"] :@"capture" :[dict objectForKey:@"transaction_id"] :amount];
                                                               
                                                               PFUser *currentUser = [PFUser currentUser];
                                                               PFQuery *query = [PFQuery queryWithClassName:@"_User"];
                                                               [query getObjectInBackgroundWithId:currentUser.objectId block:^(PFObject *gameScore, NSError *error) {
                                                                   
                                                                   NSString *transportNumber = gameScore[@"transportNumber"];
                                                                   NSString *bankingNumber = gameScore[@"bankingNumber"];

                                                                   PFQuery *query = [PFQuery queryWithClassName:@"UKTicket"];
                                                                   [query getObjectInBackgroundWithId:transportNumber block:^(PFObject *gameScore, NSError *error) {
                                                                       NSString *strRemote = gameScore[@"currentCredit"];
                                                                       float a = [strRemote floatValue];
                                                                       
                                                                       NSString *strLocal = _amountEntered.text;
                                                                       float b = [strLocal floatValue];
                                                                       
                                                                       PFQuery *query = [PFQuery queryWithClassName:@"UKTicket"];
                                                                       [query getObjectInBackgroundWithId:transportNumber
                                                                                                    block:^(PFObject *gameScore, NSError *error) {
                                                                                                        float c = a + b;
                                                                                                        NSString *strFinal = [NSString stringWithFormat:@"%f", c];
                                                                                                        gameScore[@"currentCredit"] = strFinal;
                                                                                                        [gameScore saveInBackground];
                                                                                                        
                                                                                                        PFQuery *query = [PFQuery queryWithClassName:@"LLOYBanking"];
                                                                                                        [query getObjectInBackgroundWithId:bankingNumber block:^(PFObject *gameScore, NSError *error) {
                                                                                                            NSString *currentCreditInDB = gameScore[@"currentCredit"];
                                                                                                            float currentCreditFloat = [currentCreditInDB floatValue];
                                                                                                            
                                                                                                            PFQuery *query = [PFQuery queryWithClassName:@"LLOYBanking"];
                                                                                                            [query getObjectInBackgroundWithId:bankingNumber
                                                                                                                                         block:^(PFObject *gameScore, NSError *error) {
                                                                                                                                             float transferAmount = currentCreditFloat - b;
                                                                                                                                             NSString *newBankCredit = [NSString stringWithFormat:@"%f", transferAmount];
                                                                                                                                             gameScore[@"currentCredit"] = newBankCredit;
                                                                                                                                             [gameScore saveInBackground];
                                                                                                                                         }];
                                                                                                            
                                                                                                            NSLog(@"%@", gameScore);
                                                                                                        }];
                                                                                                    }];
                                                                       NSLog(@"%@", gameScore);
                                                                   }];

                                                                   
                                                               }];
                                                               
                                                           } else {
                                                               authStatusMessage = [NSString stringWithFormat:@"Error was encountered processing transaction: %@", error.debugDescription];
                                                           }
                                                           
                                                           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"First Data Payment Authorization"
                                                                                                           message:authStatusMessage delegate:self
                                                                                                 cancelButtonTitle:@"Dismiss"
                                                                                                 otherButtonTitles:nil];
                                                           [alert show];
                                                       }];
    }
}

- (IBAction)purchaseVoidTransaction:(id)sender {
    
    NSDecimalNumber* valueEntered = [NSDecimalNumber decimalNumberWithString:_amountEntered.text];
    
    if (![valueEntered isEqualToNumber:[NSDecimalNumber notANumber]]) {
        
        NSString *amount = [[NSString stringWithFormat:@"%@",valueEntered] stringByReplacingOccurrencesOfString:@"." withString:@""];
        
        // Test credit card info
        NSDictionary* credit_card = @{
                                      @"type":@"visa",
                                      @"cardholder_name":self.card_holder_name.text,
                                      @"card_number":self.card_number.text,
                                      @"exp_date":@"0416",
                                      @"cvv":self.card_security_code.text
                                      };
        PayeezySDK* myClient = [[PayeezySDK alloc]initWithApiKey:KApiKey apiSecret:KApiSecret merchantToken:KToken url:KURL];
        
        //myClient.url = KURL;
        
        [myClient submitAuthorizeTransactionWithCreditCardDetails:credit_card[@"type"] cardHolderName:credit_card[@"cardholder_name"] cardNumber:credit_card[@"card_number"] cardExpirymMonthAndYear:credit_card[@"exp_date"] cardCVV:credit_card[@"cvv"] currencyCode:@"GBP" totalAmount:amount merchantRefForProcessing:@"Test Authorization Transaction - PayeezyClient"
         
                                                       completion:^(NSDictionary *dict, NSError *error) {
                                                           
                                                           NSString *authStatusMessage = nil;
                                                           
                                                           if (error == nil)
                                                           {
                                                               authStatusMessage = [NSString stringWithFormat:@"Transaction Successful\rType:%@\rTransaction ID:%@\rTransaction Tag:%@\rCorrelation Id:%@\rBank Response Code:%@",
                                                                                    [dict objectForKey:@"transaction_type"],
                                                                                    [dict objectForKey:@"transaction_id"],
                                                                                    [dict objectForKey:@"transaction_tag"],
                                                                                    [dict objectForKey:@"correlation_id"],
                                                                                    [dict objectForKey:@"bank_resp_code"]];
                                                
                                                           } else {
                                                               authStatusMessage = [NSString stringWithFormat:@"Error was encountered processing transaction: %@", error.debugDescription];
                                                           }
                                                           
                                                           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"First Data Payment Authorization"
                                                                                                           message:authStatusMessage delegate:self
                                                                                                 cancelButtonTitle:@"Dismiss"
                                                                                                 otherButtonTitles:nil];
                                                           [alert show];
                                                           
                                                       }];
    }
}

- (IBAction)purchaseRefundTransaction:(id)sender {
    
    NSDecimalNumber* valueEntered = [NSDecimalNumber decimalNumberWithString:_amountEntered.text];
    
    if (![valueEntered isEqualToNumber:[NSDecimalNumber notANumber]]) {
        
        NSString *amount = [[NSString stringWithFormat:@"%@",valueEntered] stringByReplacingOccurrencesOfString:@"." withString:@""];
        
        // Test credit card info
        NSDictionary* credit_card = @{
                                      @"type":@"visa",
                                      @"cardholder_name":@"Eck Test 3",
                                      @"card_number":@"4012000033330026",
                                      @"exp_date":@"0416",
                                      @"cvv":@"123"
                                      };
        PayeezySDK* myClient = [[PayeezySDK alloc]initWithApiKey:KApiKey apiSecret:KApiSecret merchantToken:KToken url:KURL];
        
        //myClient.url = KURL;
        
        [myClient submitAuthorizeTransactionWithCreditCardDetails:credit_card[@"type"] cardHolderName:credit_card[@"cardholder_name"] cardNumber:credit_card[@"card_number"] cardExpirymMonthAndYear:credit_card[@"exp_date"] cardCVV:credit_card[@"cvv"] currencyCode:@"USD" totalAmount:amount merchantRefForProcessing:@"Test Authorization Transaction - PayeezyClient"
         
                                                       completion:^(NSDictionary *dict, NSError *error) {
                                                           
                                                           NSString *authStatusMessage = nil;
                                                           
                                                           if (error == nil)
                                                           {
                                                               authStatusMessage = [NSString stringWithFormat:@"Transaction Successful\rType:%@\rTransaction ID:%@\rTransaction Tag:%@\rCorrelation Id:%@\rBank Response Code:%@",
                                                                                    [dict objectForKey:@"transaction_type"],
                                                                                    [dict objectForKey:@"transaction_id"],
                                                                                    [dict objectForKey:@"transaction_tag"],
                                                                                    [dict objectForKey:@"correlation_id"],
                                                                                    [dict objectForKey:@"bank_resp_code"]];
                                                           }
                                                           else
                                                           {
                                                               authStatusMessage = [NSString stringWithFormat:@"Error was encountered processing transaction: %@", error.debugDescription];
                                                           }
                                                           
                                                           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"First Data Payment Authorization"
                                                                                                           message:authStatusMessage delegate:self
                                                                                                 cancelButtonTitle:@"Dismiss"
                                                                                                 otherButtonTitles:nil];
                                                           [alert show];
                                                       }];
    }
}

- (IBAction)voidRefundCaptureTransaction:(NSString *)merchant_ref :(NSString *)transaction_tag  :(NSString *)transaction_type   :(NSString *)transaction_id :(NSString *)totalamount  {
    
    NSDecimalNumber* valueEntered = [NSDecimalNumber decimalNumberWithString:_amountEntered.text];
    
    if (![valueEntered isEqualToNumber:[NSDecimalNumber notANumber]]) {
        
        //   NSString *amount = [[NSString stringWithFormat:@"%@",valueEntered] stringByReplacingOccurrencesOfString:@"." withString:@""];
        
        // Test credit card info
        NSDictionary* void_transaction = @{
                                           @"merchant_ref": merchant_ref,
                                           @"transaction_tag": transaction_tag,
                                           @"transaction_type": transaction_type,
                                           @"transaction_id": transaction_id,
                                           @"method": @"credit_card",
                                           @"amount": totalamount,
                                           @"currency_code": @"GBP"
                                           };
        
        NSString *vcrURL = [NSString stringWithFormat: @"%@/%@", KURL, transaction_id];
        
        PayeezySDK* myClient = [[PayeezySDK alloc]initWithApiKey:KApiKey apiSecret:KApiSecret merchantToken:KToken url:vcrURL];
        
        [myClient submitVoidCaptureRefundTransactionWithCreditCardDetails:void_transaction[@"merchant_ref"] transactiontag:void_transaction[@"transaction_tag"] transactionType:void_transaction[@"transaction_type"] transactionId:void_transaction[@"transaction_id"] paymentMethodType:void_transaction[@"method"] totalAmount:void_transaction[@"amount"] currencyCode:@"GBP" completion:^(NSDictionary *dict, NSError *error) {
            
            NSString *authStatusMessage = nil;
            
            if (error == nil)
            {
                authStatusMessage = [NSString stringWithFormat:@"Transaction Successful\rType:%@\rTransaction ID:%@\rTransaction Tag:%@",
                                     [dict objectForKey:@"transaction_type"],
                                     [dict objectForKey:@"transaction_id"],
                                     [dict objectForKey:@"transaction_tag"]];
            }
            else
            {
                authStatusMessage = [NSString stringWithFormat:@"Error was encountered processing transaction: %@", error.debugDescription];
            }
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"First Data Payment Authorization"
                                                            message:authStatusMessage delegate:self
                                                  cancelButtonTitle:@"Dismiss"
                                                  otherButtonTitles:nil];
            [alert show];
        }];
    }
}

-(NSString*) splitTransaction:(NSString *)merchant_ref :(NSString *)transaction_tag  :(NSString *)transaction_type   :(NSString *)transaction_id :(NSString *)split_shipment :(NSString *)totalamount  {
    
    NSDecimalNumber* valueEntered = [NSDecimalNumber decimalNumberWithString:_amountEntered.text];
    
    if (![valueEntered isEqualToNumber:[NSDecimalNumber notANumber]]) {
        
        //   NSString *amount = [[NSString stringWithFormat:@"%@",valueEntered] stringByReplacingOccurrencesOfString:@"." withString:@""];
        
        NSDictionary* split_transaction = @{
                                            @"merchant_ref": merchant_ref,
                                            @"transaction_tag": transaction_tag,
                                            @"transaction_type": transaction_type,
                                            @"split_shipment": split_shipment,
                                            @"method": @"credit_card",
                                            @"amount": totalamount,
                                            @"currency_code": @"GBP"
                                            };
        
        NSString *vcrURL = [NSString stringWithFormat: @"%@/%@", KURL, transaction_id];
        
        PayeezySDK* myClient = [[PayeezySDK alloc]initWithApiKey:KApiKey apiSecret:KApiSecret merchantToken:KToken url:vcrURL];
        
        [myClient submitSplitTransactionWithCreditCardDetails:split_transaction[@"merchant_ref"] transactiontag:split_transaction[@"transaction_tag"] transactionType:split_transaction[@"transaction_type"]  paymentMethodType:split_transaction[@"method"]  splitShipment:split_transaction[@"split_shipment"] totalAmount:totalamount currencyCode:@"USD" completion:^(NSDictionary *dict, NSError *error) {
            
            NSString *authStatusMessage = nil;
            
            
            if (error == nil)
            {
                authStatusMessage = [NSString stringWithFormat:@"Transaction Successful\rType:%@\rTransaction ID:%@\rTransaction Tag:%@\rCorrelation Id:%@\rBank Response Code:%@",
                                     [dict objectForKey:@"transaction_type"],
                                     [dict objectForKey:@"transaction_id"],
                                     [dict objectForKey:@"transaction_tag"],
                                     [dict objectForKey:@"correlation_id"],
                                     [dict objectForKey:@"bank_resp_code"]];
                
            }
            else
            {
                authStatusMessage = [NSString stringWithFormat:@"Error was encountered processing transaction: %@", error.debugDescription];
            }
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"First Data Payment Authorization"
                                                            message:authStatusMessage delegate:self
                                                  cancelButtonTitle:@"Dismiss"
                                                  otherButtonTitles:nil];
            [alert show];
        }];
        
    }
    return @"";
}

@end
